<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Idloom</title>
</head>
<body>

<h2>Register</h2>

<form action="{{ route('create') }}" method="post">
    @csrf
    <label for="email">E-mail address</label>
    <input type="email" name="email" id="email" required><br>

    <label for="firstname">First name</label>
    <input type="text" name="firstname" id="firstname" required><br>

    <label for="lastname">Last name</label>
    <input type="text" name="lastname" id="lastname" required><br>

    <label for="birthdate"></label>
    <input type="date" name="birthdate" id="birthdate" required><br>

    <label for="password">Password</label>
    <input type="password" name="password" required> <br>

    <label for="confirm-password">Confirm password</label>
    <input type="password" name="confirm-password" required> <br>

    <button type="submit">Sign up</button>
</form>

<h2>Log in</h2>

<form action="{{ route('profile') }}" method="post">
    @csrf
    <label for="email">E-mail address</label>
    <input type="email" name="email" id="email"><br>

    <label for="password">Password</label>
    <input type="password" name="password"> <br>

    <button type="submit">Sign in </button>
</form>

    
</body>
</html>