<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Profile</title>
</head>
<body>
    <h1>Hi, FIRSTNAME</h1>

    <h2>Edit Profile</h2>

    <form action="{{ route('update') }}" method="post">
    @method('PUT')
    @csrf
    <label for="email">E-mail address</label>
    <input type="email" name="email" id="email" value='EMAIL'><br>

    <label for="firstname">First name</label>
    <input type="text" name="firstname" id="firstname" value='FIRSTNAME'><br>

    <label for="lastname">Last name</label>
    <input type="text" name="lastname" id="lastname" value='LASTNAME'><br>

    <label for="birthdate"></label>
    <input type="date" name="birthdate" id="birthdate" value='01/01/1950'><br>

    <label for="password">Password</label>
    <input type="password" name="password"> <br>

    <label for="confirm-password">Confirm password</label>
    <input type="password" name="confirm-password"> <br>

    <button type="submit">Save</button>
    </form>

    <h2>Delete my account</h2>
    <form action="{{ route('delete') }}" method="post">
    @method('DELETE')
    @csrf
    <button type="submit">Delete my account</button>
    </form>
</body>
</html>